class Song < ApplicationRecord
  extend PCO

  attr_reader :songs

  def self.from_pco
    @connection = build_connection
    @pco_songs = self.pco_song_data
  end

  private

  def self.pco_song_data
    songs = []
    songs_url = "#{PCO::URL}services/v2/songs?per_page=100&offset=0"
    loop do
      temp = @connection.get(songs_url)
      songs = songs.concat(temp.body['data'])
      break unless temp.body['links'].key?('next')

      songs_url = temp.body.dig 'links', 'next'
    end
    self.sort_pco_songs(songs)
  end

  def self.sort_pco_songs(songs)
    songs.sort do |a,b|
      a['attributes']['title'] <=> b['attributes']['title']
    end
  end
end