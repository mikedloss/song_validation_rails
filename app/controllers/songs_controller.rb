require 'pry'

class SongsController < ApplicationController
  def index
    @songs = Song.all
    render json: @songs
  end

  def update
    pco_songs = []
    Song.from_pco.each do |song|
      Song.find_or_create_by(pco_id: song['id']) do |s|
        s.title = song['attributes']['title']
        s.author = song['attributes']['author']
        s.ccli_number = song['attributes']['ccli_number']
        s.pco_data = song
      end
    end
    
    render json: Song.all
  end

  private


end
