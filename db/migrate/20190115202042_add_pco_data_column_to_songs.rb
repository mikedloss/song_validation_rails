class AddPcoDataColumnToSongs < ActiveRecord::Migration[5.2]
  def change
    add_column :songs, :pco_data, :jsonb
  end
end
