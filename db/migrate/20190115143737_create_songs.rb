class CreateSongs < ActiveRecord::Migration[5.2]
  def change
    create_table :songs do |t|
      t.string :pco_id
      t.string :title
      t.string :ccli_number
      t.string :author
      t.timestamps
    end
  end
end
