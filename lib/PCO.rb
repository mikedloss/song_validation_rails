# frozen_string_literal: true

require 'faraday'
require 'faraday_middleware'

# Provides helper functions for PCO related services
module PCO
  URL = 'https://api.planningcenteronline.com/'

  def build_connection
    Faraday.new(url: URL) do |faraday|
      faraday.response :json
      faraday.basic_auth ENV['APP_KEY'], ENV['SECRET_KEY']
      faraday.adapter :excon
    end
  end
end
